DROP TABLE IF EXISTS SecureHashes CASCADE
;
CREATE TABLE SecureHashes ( 
	user_id integer NOT NULL,
	id serial NOT NULL,
	salt varchar(50) NOT NULL
)
;

ALTER TABLE SecureHashes
	ADD CONSTRAINT UQ_SecureHashes_id UNIQUE (id)
;
ALTER TABLE SecureHashes
	ADD CONSTRAINT UQ_SecureHashes_user_id UNIQUE (user_id)
;
ALTER TABLE SecureHashes ADD CONSTRAINT PK_SecureHashes 
	PRIMARY KEY (id)
;
select * from users
select * from securehashes
delete from users where id=12
delete from securehashes where user_id=12
delete from notifications where fk_user_id=3

--ALTER TABLE beacon ADD uuid varchar(40) NULL
ALTER TABLE Notifications ADD notification_time timestamptz NULL
ALTER TABLE Notifications ALTER COLUMN notification_time SET DEFAULT NOW()


CREATE OR REPLACE FUNCTION update_changetimestamp_column() RETURNS TRIGGER AS $$
	BEGIN
	   NEW.notification_time = NOW(); 
	   RETURN NEW;
	END;
$$ language 'plpgsql';

CREATE TRIGGER update_notifications_changetimestamp BEFORE UPDATE
    ON notifications FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
    

DROP TRIGGER update_notifications_changetimestamp ON notifications
package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model;

import java.io.Serializable;
import javax.persistence.*;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffService.utils.ServiceUserBuilder;

import java.util.Set;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USERS_ID_GENERATOR", sequenceName="users_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USERS_ID_GENERATOR")
	private Integer id;

	private String email;

	@Column(name="password_hash")
	private String passwordHash;

	//bi-directional many-to-one association to Beacon
	@OneToMany(mappedBy="user")
	private Set<Beacon> beacons;

	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="user")
	private Set<Notification> notifications;

	public User() {
	}
	
	public User(ServiceUserBuilder builder){
		this.id = builder.id;
		this.email = builder.email;
		this.passwordHash = builder.passwordHash;
		this.beacons = builder.beacons;
		this.notifications = builder.notifications;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordHash() {
		return this.passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Set<Beacon> getBeacons() {
		return this.beacons;
	}

	public void setBeacons(Set<Beacon> beacons) {
		this.beacons = beacons;
	}

	public Beacon addBeacon(Beacon beacon) {
		getBeacons().add(beacon);
		beacon.setUser(this);

		return beacon;
	}

	public Beacon removeBeacon(Beacon beacon) {
		getBeacons().remove(beacon);
		beacon.setUser(null);

		return beacon;
	}

	public Set<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(Set<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setUser(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setUser(null);

		return notification;
	}

	public static User fromJSON(String userJSON){
		return new Gson().fromJson(userJSON, User.class);
	}
}
DROP TABLE IF EXISTS Beacons CASCADE
;
DROP TABLE IF EXISTS Notifications CASCADE
;
DROP TABLE IF EXISTS SecureHashes CASCADE
;
DROP TABLE IF EXISTS Users CASCADE
;
DROP TABLE IF EXISTS Devices CASCADE
;

CREATE TABLE Beacons ( 
	id serial NOT NULL,
	device_id integer NOT NULL,
	name varchar(50) NOT NULL,
	major_id varchar(3) NULL,
	minor_id varchar(3) NULL,
	uuid varchar(40) NULL,
	fk_user_id integer NULL
)
;

CREATE TABLE Notifications ( 
	id serial NOT NULL,
	geo_coordinates_longitude double precision NOT NULL,
	message varchar(160) NULL,
	notification_time timestamptz NULL,
	geo_coordinates_latitude double precision NOT NULL,
	fk_user_id integer NULL
)
;

CREATE TABLE SecureHashes ( 
	user_id integer NOT NULL,
	id serial NOT NULL,
	salt varchar(100) NOT NULL
)
;

CREATE TABLE Users ( 
	id serial NOT NULL,
	email varchar(254) NOT NULL,
	password_hash varchar(100) NOT NULL
)
;

CREATE TABLE Devices ( 
	id serial NOT NULL,
	device_id varchar(64) NOT NULL,
	notification_priority integer NULL,
	fk_user_id integer NULL
)
;

ALTER TABLE Devices
	ADD CONSTRAINT UQ_Devices_id UNIQUE (id)
;
ALTER TABLE Beacons
	ADD CONSTRAINT UQ_Beacons_id UNIQUE (id)
;
ALTER TABLE Notifications
	ADD CONSTRAINT UQ_Notifications_id UNIQUE (id)
;
ALTER TABLE SecureHashes
	ADD CONSTRAINT UQ_SecureHashes_user_id UNIQUE (user_id)
;
ALTER TABLE SecureHashes
	ADD CONSTRAINT UQ_SecureHashes_id UNIQUE (id)
;
ALTER TABLE Users
	ADD CONSTRAINT UQ_Users_id UNIQUE (id)
;


ALTER TABLE Devices ADD CONSTRAINT PK_Devices 
	PRIMARY KEY (id)
;
ALTER TABLE Beacons ADD CONSTRAINT PK_Beacons 
	PRIMARY KEY (id)
;
ALTER TABLE Notifications ADD CONSTRAINT PK_Notifications 
	PRIMARY KEY (id)
;
ALTER TABLE SecureHashes ADD CONSTRAINT PK_SecureHashes 
	PRIMARY KEY (id)
;
ALTER TABLE Users ADD CONSTRAINT PK_Users 
	PRIMARY KEY (id)
;



ALTER TABLE Devices ADD CONSTRAINT FK_Devices_Users 
	FOREIGN KEY (fk_user_id) REFERENCES Users (id)
ON DELETE CASCADE ON UPDATE NO ACTION
;

ALTER TABLE Beacons ADD CONSTRAINT FK_Beacon_Users 
	FOREIGN KEY (fk_user_id) REFERENCES Users (id)
ON DELETE CASCADE ON UPDATE NO ACTION
;

ALTER TABLE Notifications ADD CONSTRAINT FK_Notifications_Users 
	FOREIGN KEY (fk_user_id) REFERENCES Users (id)
	ON DELETE CASCADE ON UPDATE NO ACTION
;


CREATE OR REPLACE FUNCTION update_changetimestamp_column() RETURNS TRIGGER AS $$
	BEGIN
	   NEW.notification_time = NOW(); 
	   RETURN NEW;
	END;
$$ language 'plpgsql';

CREATE TRIGGER update_notifications_changetimestamp BEFORE UPDATE
    ON notifications FOR EACH ROW EXECUTE PROCEDURE 
    update_changetimestamp_column();
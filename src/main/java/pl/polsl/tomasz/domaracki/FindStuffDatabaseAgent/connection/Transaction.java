package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection;

import javax.persistence.EntityManager;


public class Transaction {

	private EntityManager em;
	
	public Transaction(EntityManager em) {
		this.em = em;
	}
	
	@Override
	protected void finalize() throws Throwable {
		if(em != null && em.getTransaction().isActive()){
			em.getTransaction().rollback();
		}
		super.finalize();
	}
	
	public void begin(){
		em.getTransaction().begin();
	}
	
	public void commit(){
		em.getTransaction().commit();
	}
	
	public void rollback(){
		em.getTransaction().rollback();
	}
}

package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection;

import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;

public class ConnectionManager {

	private Transaction transaction;
	
	public Transaction getTransaction() {
		return transaction;
	}
	private static EntityManagerFactory emFactory;
	private EntityManager manager;
	private final String databaseUrl = "postgres://ycboaxjcijwqwp:vQ7S8qpVRjWaSTaWWlXkurK3W3@ec2-54-228-180-92.eu-west-1.compute.amazonaws.com:5432/d7tjlusadl9n30";
	
	private HashMap<String, String> getConnectionProperties(){
        StringTokenizer st = new StringTokenizer(databaseUrl, ":@/");
        String dbVendor = st.nextToken();
        String userName = st.nextToken();
        String password = st.nextToken();
        String host = st.nextToken();
        String port = st.nextToken();
        String databaseName = st.nextToken();
        String jdbcUrl = String.format("jdbc:postgresql://%s:%s/%s?ssl=true&sslfactory=org.postgresql.ssl.NonValidatingFactory", host, port, databaseName);
        HashMap<String, String> properties = new HashMap<String, String>();
        properties.put("javax.persistence.jdbc.url", jdbcUrl );
        properties.put("javax.persistence.jdbc.user", userName );
        properties.put("javax.persistence.jdbc.password", password );
        properties.put("javax.persistence.jdbc.driver", "org.postgresql.Driver");
        properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        return properties;
	}
	
	@Override
	protected void finalize() throws Throwable {
		closeEntityManager();
		super.finalize();
	}
	
	private EntityManagerFactory prepareEntityManagerFactory(){
		if(emFactory == null){
			HashMap<String, String> properties = getConnectionProperties();
			emFactory = Persistence.createEntityManagerFactory("FindStuffService", properties);
		}
		return emFactory;
	}
	
	public ConnectionManager(){
		prepareEntityManagerFactory();
		manager = emFactory.createEntityManager();
		transaction = new Transaction(manager);
	}
	
	public <T> void persist(Class<T> entityClass, Object toAdd){
		transaction.begin();
		manager.persist(entityClass.cast(toAdd));
		transaction.commit();		
	}
	
	public <T> T merge(Class<T> entityClass, Object toUpdate){
		transaction.begin();
		T updated = manager.merge(entityClass.cast(toUpdate));
		transaction.commit();
		return updated;
	}
	
	public <T> void remove(Class<T> entityClass, Object toRemove){
		transaction.begin();
		manager.remove(entityClass.cast(toRemove));
		transaction.commit();
	}
	
	public <T> Object findByClassAndPK(Class<T> entityClass, int pk){
		return manager.find(entityClass, pk);
	}
	
	public List executeGetQuery(String queryString){
		Query getQuery = manager.createQuery(queryString);
		List result = getQuery.getResultList();
		return result;
	}
	
	public void closeEntityManager(){
		if(manager != null){
			manager.close();
		}
	}
	
	public User refreshUser(User toRefresh){
		manager.refresh(toRefresh);
		return toRefresh;
	}
	
	public Beacon refreshBeacon(Beacon toRefresh){
		manager.refresh(toRefresh);
		return toRefresh;
	}
}

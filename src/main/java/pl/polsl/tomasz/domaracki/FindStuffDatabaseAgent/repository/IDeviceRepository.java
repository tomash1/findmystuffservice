package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;

public interface IDeviceRepository {

	Device addDevice(Device deviceToAdd);
	
	Device getDeviceById(Integer id);
	
	boolean removeDevice(Device deviceToRemove);
	
}

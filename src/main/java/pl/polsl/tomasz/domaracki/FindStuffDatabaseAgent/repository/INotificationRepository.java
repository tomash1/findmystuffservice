package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;

public interface INotificationRepository {

	Notification addNotification(Notification notificationToAdd);
	
	Notification getNotificationById(Integer id);
	
	Notification updateNotification(Notification toUpdate);
	
	boolean removeNotification(Notification notificationToAdd);
	
}

package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the securehashes database table.
 * 
 */
@Entity
@Table(name="securehashes")
@NamedQuery(name="SecureHash.findAll", query="SELECT s FROM SecureHash s")
public class SecureHash implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SECUREHASHES_ID_GENERATOR", sequenceName="securehashes_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SECUREHASHES_ID_GENERATOR")
	private Integer id;

	private String salt;

	@Column(name="user_id")
	private Integer userId;

	public SecureHash() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
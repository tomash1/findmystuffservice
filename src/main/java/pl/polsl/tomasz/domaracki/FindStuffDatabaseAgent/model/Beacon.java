package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the beacons database table.
 * 
 */
@Entity
@Table(name="beacons")
@NamedQuery(name="Beacon.findAll", query="SELECT b FROM Beacon b")
public class Beacon implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="BEACONS_ID_GENERATOR", sequenceName="beacons_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="BEACONS_ID_GENERATOR")
	private Integer id;

	@Column(name="device_id")
	private Integer deviceId;

	@Column(name="major_id")
	private String majorId;

	@Column(name="minor_id")
	private String minorId;

	private String name;

	private String uuid;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="fk_user_id")
	private User user;

	public Beacon() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public String getMajorId() {
		return this.majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getMinorId() {
		return this.minorId;
	}

	public void setMinorId(String minorId) {
		this.minorId = minorId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation;

import java.util.List;

import javax.validation.UnexpectedTypeException;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.ConnectionManager;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IBeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.ISaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;

public class BeaconRepository implements IBeaconRepository {

	private ConnectionManager connectionManager;
//	private final String SQLGetBeaconByCredentials = "SELECT b FROM Beacon b WHERE b.uuid='%s' AND"
//																				+ " b.minorId='%s' AND"
//																				+ " b.majorId='%s'";

	private final String SQLGetBeaconByCredentials = "SELECT b FROM Beacon b";
	public BeaconRepository() {
		connectionManager = new ConnectionManager();
	}
	
	@Override
	public Beacon addBeacon(Beacon beaconToAdd) {
		if(beaconToAdd == null){
			return null;
		}
		connectionManager.persist(Beacon.class, beaconToAdd);
		
		return beaconToAdd;
	}

	public boolean removeBeacon(Beacon beaconToRemove){
		if (beaconToRemove == null){
			return false;
		}
		
		Integer id = beaconToRemove.getId();
		if(id == null){
			return false;
		}
		
		Beacon beaconToRemoveInDb = (Beacon)connectionManager.findByClassAndPK(Beacon.class, beaconToRemove.getId());
		if (beaconToRemoveInDb == null) {
			return false;
		}
		
		connectionManager.remove(Beacon.class, beaconToRemoveInDb);
		return true;
	}

	@Override
	public Beacon getBeaconById(Integer id) throws WrongParameterException {
		if(id == null) { 
			return null;
		}
		Beacon beacon = (Beacon)connectionManager.findByClassAndPK(Beacon.class, id);
		if (beacon == null){
			throw new WrongParameterException("Given beacon was not found in database.");
		}
		return beacon;
	}

	@Override
	public Beacon getBeaconByCredentials(String uuid, String majorId, String minorId) {
		List beacons = connectionManager.executeGetQuery(String.format(SQLGetBeaconByCredentials));

		for(Object beaconObj : beacons){
			Beacon beacon = (Beacon)beaconObj;
			if(beacon == null){
				throw new UnexpectedTypeException("Beacon is null when getting by credentials");
			}
			if(!beacon.getUuid().equals(uuid.toLowerCase()) || !beacon.getMajorId().equals(majorId) || !beacon.getMinorId().equals(minorId)){
				continue;
			}
			connectionManager.refreshBeacon(beacon);
			return beacon;
		}
		return null;
	}
	
}

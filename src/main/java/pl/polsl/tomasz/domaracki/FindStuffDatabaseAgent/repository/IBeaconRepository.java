package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;

public interface IBeaconRepository {

	Beacon addBeacon(Beacon beaconToAdd);
	
	Beacon getBeaconById(Integer id) throws WrongParameterException;
	
	boolean removeBeacon(Beacon beaconToRemove);
	
	Beacon getBeaconByCredentials(String uuid, String majorId, String minorId);
}

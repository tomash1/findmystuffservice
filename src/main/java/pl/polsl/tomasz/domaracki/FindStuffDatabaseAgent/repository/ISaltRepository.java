package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.SecureHash;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;

public interface ISaltRepository {

	SecureHash getHashByUser(User userToGetSalt);
	
	User addSaltToPassword(User userWithoutSalt);
}

package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation;

import java.util.List;

import javax.validation.UnexpectedTypeException;

import com.sun.org.apache.bcel.internal.generic.SALOAD;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.ConnectionManager;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.Transaction;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.SecureHash;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.ISaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;

public class UserRepository implements IUserRepository {

	private ConnectionManager connectionManager;
	private final String SQLGetUserByEmail = "SELECT u FROM User u WHERE u.email=\"%s\"";
	
	public UserRepository() {
		connectionManager = new ConnectionManager();
	}
	
	public User updateUser(User userToUpdate) {
		if(userToUpdate == null){
			return null;
		}
		User updatedUser = connectionManager.merge(User.class, userToUpdate);
		return updatedUser;
	}
	
	@Override
	public User addUser(User userToAdd) {
		if(userToAdd == null){
			return null;
		}
		User userWithGivenMail = getUserByEmail(userToAdd.getEmail());
		if(userWithGivenMail != null){
			return null;
		}
		connectionManager.persist(User.class, userToAdd);
		
		ISaltRepository salts = new SaltRepository();		
		User userToAddWithSalt = salts.addSaltToPassword(userToAdd);
		if (userToAddWithSalt == null){
			return null;
		}
		return userToAddWithSalt;
	}

	@Override
	public User getUserByEmail(String email) {
		//TODO validate email for SQLInj
		List users = connectionManager.executeGetQuery(String.format(SQLGetUserByEmail, email));

		if(users.size() == 1){
			User user = (User)users.get(0);
			if(user == null){
				throw new UnexpectedTypeException("User is null when getting by email");
			}
			connectionManager.refreshUser(user);
			return user;
		}
		return null;
	}

	@Override
	public boolean resetGivenUserPassword(User userWithNewPassword) {
		if (userWithNewPassword == null){
			return false;
		}
		List users = connectionManager.executeGetQuery(String.format(SQLGetUserByEmail, userWithNewPassword.getEmail()));
		if(users.size() == 1){
			User user = (User)users.get(0);
			if(user == null){
				throw new UnexpectedTypeException("User is null when trying to reset password");
			}
			Transaction transaction = connectionManager.getTransaction();
			transaction.begin();
			user.setPasswordHash(userWithNewPassword.getPasswordHash());
			transaction.commit();
			return true;
		}
		return false;
	}

	@Override
	public boolean removeUser(User userToRemove) {
		if (userToRemove == null){
			return false;
		}
		
		Integer id = userToRemove.getId();
		if(id == null){
			return false;
		}
		SecureHash saltToRemove = new SaltRepository().getHashByUser(userToRemove);
		if(saltToRemove == null){
			return false;
		}
		SecureHash saltToRemoveInDb = (SecureHash)connectionManager.findByClassAndPK(SecureHash.class, saltToRemove.getId());
		User userToRemoveInDb = (User)connectionManager.findByClassAndPK(User.class, userToRemove.getId());
		if(userToRemoveInDb == null || saltToRemoveInDb == null){
			return false;
		}
		
		connectionManager.remove(User.class, userToRemoveInDb);
		connectionManager.remove(SecureHash.class, saltToRemoveInDb);

		return true;
	}
}

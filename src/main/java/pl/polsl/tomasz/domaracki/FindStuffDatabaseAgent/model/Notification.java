package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the notifications database table.
 * 
 */
@Entity
@Table(name="notifications")
@NamedQuery(name="Notification.findAll", query="SELECT n FROM Notification n")
public class Notification implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="NOTIFICATIONS_ID_GENERATOR", sequenceName="notifications_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="NOTIFICATIONS_ID_GENERATOR")
	private Integer id;

	@Column(name="geo_coordinates_latitude")
	private double geoCoordinatesLatitude;

	@Column(name="geo_coordinates_longitude")
	private double geoCoordinatesLongitude;

	private String message;

	@Column(name="notification_time")
	private Timestamp notificationTime;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="fk_user_id")
	private User user;

	public Notification() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getGeoCoordinatesLatitude() {
		return this.geoCoordinatesLatitude;
	}

	public void setGeoCoordinatesLatitude(double geoCoordinatesLatitude) {
		this.geoCoordinatesLatitude = geoCoordinatesLatitude;
	}

	public double getGeoCoordinatesLongitude() {
		return this.geoCoordinatesLongitude;
	}

	public void setGeoCoordinatesLongitude(double geoCoordinatesLongitude) {
		this.geoCoordinatesLongitude = geoCoordinatesLongitude;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Timestamp getNotificationTime() {
		return this.notificationTime;
	}

	public void setNotificationTime(Timestamp notificationTime) {
		this.notificationTime = notificationTime;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
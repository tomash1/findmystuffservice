package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the devices database table.
 * 
 */
@Entity
@Table(name="devices")
@NamedQuery(name="Device.findAll", query="SELECT d FROM Device d")
public class Device implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DEVICES_ID_GENERATOR", sequenceName="devices_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DEVICES_ID_GENERATOR")
	private Integer id;

	@Column(name="device_id")
	private String deviceId;

	@Column(name="notification_priority")
	private Integer notificationPriority;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="fk_user_id")
	private User user;

	public Device() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getNotificationPriority() {
		return this.notificationPriority;
	}

	public void setNotificationPriority(Integer notificationPriority) {
		this.notificationPriority = notificationPriority;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation;

import java.security.SecureRandom;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.ConnectionManager;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.SecureHash;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.ISaltRepository;

public class SaltRepository implements ISaltRepository{

	private ConnectionManager connectionManager;
	private final String SQLGetSaltByUserId = "SELECT s FROM SecureHash s WHERE s.userId=%d";
	
	public SaltRepository(){
		connectionManager = new ConnectionManager();
	}
	
	@Override
	public SecureHash getHashByUser(User userToGetSalt) {
		List secureHashes = connectionManager.executeGetQuery(String.format(SQLGetSaltByUserId, userToGetSalt.getId()));
		String salt;
		
		if(secureHashes.size() != 1){
			return null;
		}
		SecureHash secureHash = (SecureHash)secureHashes.get(0);
		return secureHash;
	}

	@Override
	public User addSaltToPassword(User userWithoutSalt) {
		Integer id = userWithoutSalt.getId();
		List secureHashes = null;
		if(id != null){
			secureHashes = connectionManager.executeGetQuery(String.format(SQLGetSaltByUserId, userWithoutSalt.getId()));
		}
		
		String salt;
		if(secureHashes != null && secureHashes.size() == 1){
			SecureHash secureHash = (SecureHash)secureHashes.get(0);
			salt = secureHash.getSalt();
		}
		else{
			salt = generateSalt();
			SecureHash saltHash = new SecureHash();
			saltHash.setSalt(salt);
			saltHash.setUserId(userWithoutSalt.getId());
			connectionManager.persist(SecureHash.class, saltHash);
		}
		
		if(salt == null){
			return null;
		}
		return userWithoutSalt;
	}

	private String generateSalt(){
		 byte[] salt = new byte[20];
		 new SecureRandom().nextBytes(salt);
		 String saltString = DigestUtils.sha256Hex(salt);
		 return saltString;
	}
}

package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;

public interface IUserRepository {

	User addUser(User userToAdd);
	
	User updateUser(User userToUpdate);
	
	User getUserByEmail(String email);
	
	boolean resetGivenUserPassword(User userWithNewPassword);
	
	boolean removeUser(User userToRemove);
	
}

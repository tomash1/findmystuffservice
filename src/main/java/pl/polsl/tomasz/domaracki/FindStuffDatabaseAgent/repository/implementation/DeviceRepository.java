package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.ConnectionManager;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IDeviceRepository;

public class DeviceRepository implements IDeviceRepository {

	private ConnectionManager connectionManager;
	
	public DeviceRepository() {
		connectionManager = new ConnectionManager();
	}
	
	@Override
	public Device addDevice(Device deviceToAdd) {
		if(deviceToAdd == null){
			return null;
		}
		connectionManager.persist(Device.class, deviceToAdd);
		
		return deviceToAdd;
	}

	@Override
	public Device getDeviceById(Integer id) {
		if(id == null) { 
			return null;
		}
		Device device = (Device)connectionManager.findByClassAndPK(Device.class, id);
		return device;
	}

	@Override
	public boolean removeDevice(Device deviceToRemove) {
		if (deviceToRemove == null){
			return false;
		}
		
		Integer id = deviceToRemove.getId();
		if(id == null){
			return false;
		}
		
		Device deviceToRemoveInDb = (Device)connectionManager.findByClassAndPK(Device.class, deviceToRemove.getId());
		if (deviceToRemoveInDb == null) {
			return false;
		}
		
		connectionManager.remove(Device.class, deviceToRemoveInDb);
		return true;
	}

}

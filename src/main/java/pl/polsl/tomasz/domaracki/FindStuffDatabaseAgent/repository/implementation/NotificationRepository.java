package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.connection.ConnectionManager;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.INotificationRepository;

public class NotificationRepository implements INotificationRepository {

	private ConnectionManager connectionManager;
	
	public NotificationRepository() {
		connectionManager = new ConnectionManager();
	}
	
	@Override
	public Notification addNotification(Notification notificationToAdd) {
		if(notificationToAdd == null){
			return null;
		}
		connectionManager.persist(Notification.class, notificationToAdd);
		
		return notificationToAdd;
	}

	@Override
	public Notification getNotificationById(Integer id) {
		if(id == null) { 
			return null;
		}
		Notification notification = (Notification)connectionManager.findByClassAndPK(Notification.class, id);
		return notification;
	}

	@Override
	public boolean removeNotification(Notification notificationToRemove) {
		if (notificationToRemove == null){
			return false;
		}
		
		Integer id = notificationToRemove.getId();
		if(id == null){
			return false;
		}
		
		Notification notificationToRemoveInDb = (Notification)connectionManager.findByClassAndPK(Notification.class, notificationToRemove.getId());
		if (notificationToRemoveInDb == null) {
			return false;
		}
		
		connectionManager.remove(Notification.class, notificationToRemoveInDb);
		return true;
	}

	@Override
	public Notification updateNotification(Notification toUpdate) {
		if(toUpdate == null){
			return null;
		}
		
		Notification updated = connectionManager.merge(Notification.class, toUpdate);
		return updated;
	}

}

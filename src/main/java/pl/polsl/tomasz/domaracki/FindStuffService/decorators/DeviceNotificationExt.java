package pl.polsl.tomasz.domaracki.FindStuffService.decorators;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffService.model.DeviceNotification;

public class DeviceNotificationExt {
	
	private DeviceNotification notification;
	
	public DeviceNotificationExt(DeviceNotification notification){
		this.notification = notification;
	}
	
	public DeviceNotificationExt(DeviceNotificationExt notificationExt){
		this.notification = notificationExt.notification;
	}
	
	public String toJSONString(){
		String jsonBeacon = new Gson().toJson(notification);
		return jsonBeacon;
	}

}

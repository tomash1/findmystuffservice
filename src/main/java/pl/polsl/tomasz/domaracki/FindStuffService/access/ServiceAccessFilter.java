package pl.polsl.tomasz.domaracki.FindStuffService.access;
import java.io.IOException;
import java.util.Enumeration;

import javax.naming.ConfigurationException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import pl.polsl.tomasz.domaracki.FindStuffService.utils.ResponseBuilder;

public class ServiceAccessFilter implements Filter {

	private FilterConfig config;
	
	@Override
	public void destroy() {
				
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		Authenticator authenticator = new Authenticator(req);
		if(authenticator.isCreateUserPath()) { 
			chain.doFilter(req, res); 
		} else {
			try {
				boolean requestWithValidToken = authenticator.authenticateToken();
				if( requestWithValidToken ){
					chain.doFilter(req, res);
					return;
				}
				else{
					res = new ResponseBuilder(res).getNoAuthorizationResponse();
					return;
				}
			} catch (ConfigurationException e) {/*IGNORE*/}
			
			boolean authenticationPassed = authenticator.authenticate();
			if( authenticationPassed ){
				chain.doFilter(req, res);			
			} else {
				res = new ResponseBuilder(res).getNoAuthorizationResponse();
			}
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.decorators;

import java.util.Set;
import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;

public class UserExt extends User {

	private User _user;
	
	public UserExt(User user){
		_user = user;
	}
	
	public UserExt(UserExt userExt){
		_user = userExt._user;
	}
	
	public String toJSONString(){
		UserWithEmptyValues userForJson = new UserWithEmptyValues(_user);
		return new Gson().toJson(userForJson);
	}
	
	static public User modelFromJSON(String userJSON){
		return new Gson().fromJson(userJSON, User.class);
	}
}

class UserWithEmptyValues {
	private Integer id;
	private String email;
	private String passwordHash;
	private Integer beacons;
	private Integer notifications;
	private Integer devices;
	
	public UserWithEmptyValues(User user) {
		id = user.getId();
		email = user.getEmail();
		passwordHash = user.getPasswordHash();
		beacons = user.getBeacons().size();
		notifications = user.getNotifications().size();
		devices = user.getDevices().size();
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswordHash() {
		return passwordHash;
	}
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}
	public Integer getBeacons() {
		return beacons;
	}

	public void setBeacons(Integer beacons) {
		this.beacons = beacons;
	}

	public Integer getNotifications() {
		return notifications;
	}

	public void setNotifications(Integer notifications) {
		this.notifications = notifications;
	}

	public Integer getDevices() {
		return devices;
	}

	public void setDevices(Integer devices) {
		this.devices = devices;
	}
	
}

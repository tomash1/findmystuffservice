package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IBeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.BeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.DeviceNotificationExt;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;
import pl.polsl.tomasz.domaracki.FindStuffService.model.DeviceNotification;
import pl.polsl.tomasz.domaracki.FindStuffService.notifications.PushNotificationsService;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.BeaconsNotificationsManager;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.PathsManager;

@Path("devicenotifications")
public class DeviceNotificationResource {	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getNotifications(){
		return new BeaconsNotificationsManager().getActiveGlobaNotificationsJSONString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String postNotification(String deviceNotificationJSON){
		DeviceNotification notification = addNotification(deviceNotificationJSON);
		if(notification != null){
			return new DeviceNotificationExt(notification).toJSONString();
		}
		return null;
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{beaconId}")
	public Response deleteNotification(@PathParam("beaconId") Integer beaconId){
		removeNotification(beaconId);
		return Response.ok().build();
	}
	

	private DeviceNotification addNotification(String notificationJSON){		
		DeviceNotification notification;
		try {
			notification = DeviceNotification.fromJSON(notificationJSON);
			
			BeaconsNotificationsManager manager = new BeaconsNotificationsManager();
			manager.addNewNotificationToGlobalSearch(notification);
			return notification;
		} catch (WrongParameterException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private void removeNotification(Integer beaconId){		
		BeaconsNotificationsManager manager = new BeaconsNotificationsManager();
		manager.removeNotificationFromGlobalSearch(beaconId);
	}
}

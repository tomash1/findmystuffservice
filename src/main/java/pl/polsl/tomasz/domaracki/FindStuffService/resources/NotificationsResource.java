package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IBeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.INotificationRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.BeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.NotificationRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.DeviceNotificationExt;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.NotificationExt;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;
import pl.polsl.tomasz.domaracki.FindStuffService.model.DeviceNotification;
import pl.polsl.tomasz.domaracki.FindStuffService.model.FoundNotification;
import pl.polsl.tomasz.domaracki.FindStuffService.notifications.PushNotificationsService;
@Path("notifications")
public class NotificationsResource {
	
	private IUserRepository usersRepo = new UserRepository();
	private INotificationRepository notificationsRepo = new NotificationRepository();
	private IBeaconRepository beaconRepo = new BeaconRepository();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public String getNotification(@PathParam("userEmail") String userEmail) {
		if(userEmail == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; } 
		
		Set<Notification> notifications = user.getNotifications();
		List<String> jsonNotifications = new ArrayList<>();
		for(Notification notification : notifications) {
			notification = notificationsRepo.updateNotification(notification);
			jsonNotifications.add(new NotificationExt(notification).toJSONString());
		}
		JsonElement element = new Gson().toJsonTree(jsonNotifications);

		if (! element.isJsonArray()) {
		    String fail = "";
		}
		JsonArray jsonArray = element.getAsJsonArray();
		String arrayStr = jsonArray.toString(); 
		return arrayStr;
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response foundBeaconNotification(String foundNotificationJSON){
		Notification notification = null;
		try {
			notification = addFoundNotification(foundNotificationJSON);
		} catch (WrongParameterException e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		
		if(notification != null){
			return Response.ok(new NotificationExt(notification).toJSONString()).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}/{notificationId}")
	public Response deleteNotification(@PathParam("userEmail") String userEmail, @PathParam("notificationId") Integer notificationId){
		if(userEmail == null || notificationId == null) { return Response.status(Response.Status.BAD_REQUEST).build(); }
		
		userEmail = Base64.decodeAsString(userEmail);
		User userToRemoveNotification = usersRepo.getUserByEmail(userEmail);
		if(userToRemoveNotification == null) { return Response.status(Response.Status.NOT_FOUND).build(); }
		
		Notification notification = notificationsRepo.getNotificationById(notificationId);
		if (notification == null) { return Response.status(Response.Status.NOT_FOUND).build(); }
		if (notification.getUser().getId() != userToRemoveNotification.getId()) { return Response.status(Response.Status.NOT_FOUND).build(); }
		
		userToRemoveNotification.removeNotification(notification);
		boolean result = notificationsRepo.removeNotification(notification);
		
		if(result) { return Response.status(Response.Status.OK).build(); }
		return Response.status(Response.Status.NO_CONTENT).build();
	}
	
	
	private Notification addFoundNotification(String foundNotificationJSON) throws WrongParameterException{
		FoundNotification foundNotification = FoundNotification.fromJSON(foundNotificationJSON);
		
		Beacon beacon = beaconRepo.getBeaconByCredentials(foundNotification.getUuid(), 
															foundNotification.getMajorId(), 
															foundNotification.getMinorId());
		if(beacon==null){
			throw new WrongParameterException("Found notification beacon cannot be found in database.");
		}
		
		User foundNotificationUser = beacon.getUser();
		if(foundNotificationUser == null){
			throw new WrongParameterException("Found notification beacon cannot be found in database.");
		}
		Notification notification = foundNotification.getNotification();
		notification.setMessage(beacon.getName());
		
		for(Notification userNotification : foundNotificationUser.getNotifications()){
			if(notification.getMessage().equals(userNotification.getMessage())){
				return updateNotification(notification, userNotification);
			}
		}
		
		notification.setUser(foundNotificationUser);
		notificationsRepo.addNotification(notification);
		
		foundNotificationUser.addNotification(notification);
		usersRepo.updateUser(foundNotificationUser);
		
		PushNotificationsService pushService = new PushNotificationsService(foundNotificationUser);
		pushService.sendFoundStuffNotification(notification.getMessage());
		return notification;
	}
	
	private Notification updateNotification(Notification notificationWithNewLocation, Notification toUpdate){
		toUpdate.setGeoCoordinatesLatitude( notificationWithNewLocation.getGeoCoordinatesLatitude() );
		toUpdate.setGeoCoordinatesLongitude( notificationWithNewLocation.getGeoCoordinatesLongitude() );
		
		Notification updated = notificationsRepo.updateNotification(toUpdate);
		return updated;
	}
}

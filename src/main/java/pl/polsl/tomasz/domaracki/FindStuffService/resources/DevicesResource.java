package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IDeviceRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.DeviceRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.DeviceExt;

@Path("devices")
public class DevicesResource {
	
	private IUserRepository usersRepo = new UserRepository();
	private IDeviceRepository deviceRepo = new DeviceRepository();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public String getUserDevices(@PathParam("userEmail") String userEmail){
		if(userEmail == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; }
		
		Set<Device> devices = user.getDevices();
		List<String> jsonDevices = new ArrayList<>();
		for(Device device : devices){
			jsonDevices.add(new DeviceExt(device).toJSONString());
		}
		JsonElement element = new Gson().toJsonTree(jsonDevices);
		if (! element.isJsonArray()) {
			// fail appropriately
		    String fail = "";
		}
		JsonArray jsonArray = element.getAsJsonArray();
		String arrayStr = jsonArray.toString(); 
		return arrayStr;
 	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public Response addNewDevice(String deviceJSON, @PathParam("userEmail") String userEmail){
		if(userEmail == null || deviceJSON == null) { return Response.status(Response.Status.BAD_REQUEST).build(); }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return Response.status(Response.Status.NOT_FOUND).build(); }
		
		Device device = DeviceExt.modelFromJSON(deviceJSON);
		boolean deviceExists = false;
		for(Device userDevice : user.getDevices()){
			if(userDevice.getDeviceId().equals(device.getDeviceId())){
				deviceExists = true;
			}
		}
		if(! deviceExists){
			device.setUser(user);
			
			Device addedDevice = deviceRepo.addDevice(device);
			user.addDevice(addedDevice);
			usersRepo.updateUser(user);
			
			if (addedDevice != null){
				return Response.status(Response.Status.OK).build();
			}
			return Response.noContent().build();
		}
		return Response.status(Response.Status.OK).build();
	}
}

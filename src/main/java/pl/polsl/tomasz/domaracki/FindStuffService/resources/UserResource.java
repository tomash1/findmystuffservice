package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;
import org.json.simple.parser.*;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.UserExt;

@Path("users")
public class UserResource {

	private IUserRepository usersRepo = new UserRepository();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers(){
		return "cannot get all users";
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public String getUser(@PathParam("userEmail") String userEmail){
		if(userEmail == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; } 

		return new UserExt(user).toJSONString();
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUser(String userJSON){
		if(userJSON == null) { return null; }
		
		User userToAddObj = UserExt.modelFromJSON(userJSON);
		if(userToAddObj == null) { return Response.status(Status.BAD_REQUEST).build(); }
		userToAddObj.setPasswordHash( DigestUtils.sha256Hex(DigestUtils.sha256( userToAddObj.getPasswordHash() )));
		User addedUser = usersRepo.addUser(userToAddObj);
		if(addedUser == null) { return Response.serverError().build(); }
		
		return Response.ok().build();
	}
	
	@DELETE
	@Path("/{userEmail}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@PathParam("userEmail") String userEmail){
		if(userEmail == null) { return Response.status(Response.Status.BAD_REQUEST).build(); }
		
		userEmail = Base64.decodeAsString(userEmail);
		User userToRemove = usersRepo.getUserByEmail(userEmail);
		if(userToRemove == null) { return Response.status(Response.Status.NOT_FOUND).build(); }
		
		boolean result = usersRepo.removeUser(userToRemove);
		if(result) { return Response.status(Response.Status.OK).build(); }
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}

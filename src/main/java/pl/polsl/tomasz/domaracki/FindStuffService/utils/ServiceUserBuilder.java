package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import java.util.List;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;

public class ServiceUserBuilder {
	
	public Integer id;
	public String email;
	public String passwordHash;
	public Set<Beacon> beacons;
	public Set<Notification> notifications;
	public Set<Device> devices;
	
	public ServiceUserBuilder withId(Integer id){
		this.id = id;
		return this;
	}
	
	public ServiceUserBuilder withEmail(String email){
		this.email = email;
		return this;
	}
	
	public ServiceUserBuilder withPasswordHash(String hash){
		this.passwordHash = hash;
		return this;
	}
	
	public ServiceUserBuilder withStringPass(String pass){
		this.passwordHash = DigestUtils.sha256Hex(DigestUtils.sha256(pass));
		return this;
	}
	
	public ServiceUserBuilder withBeacons(Set<Beacon> beacons){
		this.beacons = beacons;
		return this;
	}
	
	public ServiceUserBuilder withNotifications(Set<Notification> notifications){
		this.notifications = notifications;
		return this;
	}
	
	public ServiceUserBuilder withDevices(Set<Device> devices){
		this.devices = devices;
		return this;
	}
	
	public User build(){
		validate();
		return new User(this);
	}
	
	private void validate(){
		
	}
}

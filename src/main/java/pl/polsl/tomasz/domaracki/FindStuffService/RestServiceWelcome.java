package pl.polsl.tomasz.domaracki.FindStuffService;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.codec.digest.DigestUtils;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;

/**
 * Root resource (exposed at "findstuffservice" path)
 */
@Path("findstuffservice")
public class RestServiceWelcome {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
    	
//    	User newUser = new User();
//    	newUser.setEmail("asd");
//    	//newUser.setId(19);
//    	newUser.setNotifications(null);
//    	newUser.setBeacons(null);
//    	byte [] pass = DigestUtils.sha256("asd");
//    	String passString = DigestUtils.sha256Hex(pass);
//    	newUser.setPasswordHash(passString);
//    	
//    	IUserRepository userRepository = new UserRepository();
//    	userRepository.addUser(newUser);
//    	userRepository.removeUser(newUser);
    	
        return "Welcome to FindMyStuffService";
    }
}

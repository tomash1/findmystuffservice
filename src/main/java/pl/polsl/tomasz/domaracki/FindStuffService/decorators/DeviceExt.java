package pl.polsl.tomasz.domaracki.FindStuffService.decorators;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;
import pl.polsl.tomasz.domaracki.FindStuffService.model.DeviceWithoutUserObj;

public class DeviceExt {

	private Device _device;
	
	public DeviceExt(Device device){
		_device = device;
	}
	
	public DeviceExt(DeviceExt deviceExt){
		_device = deviceExt._device;
	}
	
	public String toJSONString(){
		DeviceWithoutUserObj withoutUserObjDevice = new DeviceWithoutUserObj(_device);
		String jsonDevice = new Gson().toJson(withoutUserObjDevice);
		return jsonDevice;
	}
	
	static public Device modelFromJSON(String deviceJSON){
		return new Gson().fromJson(deviceJSON, Device.class);
	}
}

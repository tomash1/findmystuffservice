package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;

import java.security.InvalidAlgorithmParameterException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.security.spec.ECGenParameterSpec;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.SecureHash;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.ISaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.SaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;

public class TokenBuilder {
	//private IUserRepository userRepo = new UserRepository();
	private ISaltRepository saltRepo = new SaltRepository();
	private User user;
	
	private static final int DEFAULT_STRENGTH = 256;
	
	public TokenBuilder(User user){
		this.user = user;
	}

	public String getUserToken(){		
		Key key = generateKey();
		if(key == null) {return null;}
		
		SecureHash hash = saltRepo.getHashByUser(user);
		if(hash == null){
			throw new UnsupportedOperationException("User does not exist");
		}
		String token = Jwts.builder()
				.setSubject(hash.getSalt())
				.signWith(SignatureAlgorithm.ES512, key)
				.compact();
		return token;
	}
	
	public boolean checkAuthorizationToken(String token){
		Key key = generateKey();
		if(key == null) {return false;}
		
		SecureHash hash = saltRepo.getHashByUser(user);
		if(hash == null){
			return false;
		}
		try{
			assert Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject().equals(hash.getSalt());
			
		} catch(SignatureException ex){ return false; }
		return true;
	}
	
	private Date prepareExpirationTime(){
		Date date = new Date();
	    long t = date.getTime();
	    Date expirationTime = new Date(t + 5 * 60 * 1000); // set 5 minutes
		return expirationTime;
	}
	
	private Key generateKey(){
		KeyPairGenerator g;
		try {
			g = KeyPairGenerator.getInstance("EC");
			g.initialize(DEFAULT_STRENGTH, new SecureRandom());
			KeyPair pair = g.generateKeyPair();
			return pair.getPrivate();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.model;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;

public class FoundNotification {

	private String uuid;
	private String majorId;
	private String minorId;
	private Notification notification;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getMajorId() {
		return majorId;
	}
	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public String getMinorId() {
		return minorId;
	}
	public void setMinorId(String minorId) {
		this.minorId = minorId;
	}
	public Notification getNotification() {
		return notification;
	}
	public void setNotification(Notification notification) {
		this.notification = notification;
	}
	
	public static FoundNotification fromJSON(String foundNotificationJSON) throws WrongParameterException{
		FoundNotification notification = new Gson().fromJson(foundNotificationJSON, FoundNotification.class);
		if(notification == null){
			throw new WrongParameterException("Failed to create found notification from given JSON String");
		}
		return notification;
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.model;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;

public class BeaconWithoutUserObj {
	
	private Integer id;
	private Integer deviceId;
	private String majorId;
	private String minorId;
	private String name;
	private String uuid;
	private Integer userId;
	
	public BeaconWithoutUserObj(Beacon beacon) {
		id 	 	 = beacon.getId();
		deviceId = beacon.getDeviceId();
		majorId  = beacon.getMajorId();
		minorId  = beacon.getMinorId();
		name 	 = beacon.getName();
		uuid 	 = beacon.getUuid();
		userId 	 = beacon.getUser().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {
		this.deviceId = deviceId;
	}

	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getMinorId() {
		return minorId;
	}

	public void setMinorId(String minorId) {
		this.minorId = minorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}

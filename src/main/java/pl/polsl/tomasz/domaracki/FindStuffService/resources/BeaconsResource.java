package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.digest.DigestUtils;
import org.glassfish.jersey.internal.util.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IBeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.BeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.BeaconExt;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;


@Path("beacons")
public class BeaconsResource {

	private IUserRepository usersRepo = new UserRepository();
	private IBeaconRepository beaconRepo = new BeaconRepository();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public String getBeacons(@PathParam("userEmail") String userEmail) {
		if(userEmail == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; } 
		
		Set<Beacon> beacons = user.getBeacons();
		List<String> jsonBeacons = new ArrayList<>();
		for(Beacon beacon : beacons) {
			jsonBeacons.add(new BeaconExt(beacon).toJSONString());
		}
		JsonElement element = new Gson().toJsonTree(jsonBeacons);

		if (! element.isJsonArray()) {
		// fail appropriately
		    String fail = "";
		}
		JsonArray jsonArray = element.getAsJsonArray();
		String arrayStr = jsonArray.toString(); 
		return arrayStr;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}")
	public String addBeacon(String beaconJSON, @PathParam("userEmail") String userEmail) {
		if(userEmail == null || beaconJSON == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; } 
		
		Beacon beacon = BeaconExt.modelFromJSON(beaconJSON);
		beacon.setUser(user);
		Beacon addedBeacon = beaconRepo.addBeacon(beacon);
		
		user.addBeacon(addedBeacon);
		usersRepo.updateUser(user);
		
		if (addedBeacon != null) {
			return new BeaconExt(addedBeacon).toJSONString();
		}
		return null;
	}
	
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{userEmail}/{beaconId}")
	public Response deleteBeacon(@PathParam("userEmail") String userEmail, @PathParam("beaconId") Integer beaconId){
		if(userEmail == null || beaconId == null) { return Response.status(Response.Status.BAD_REQUEST).build(); }
		
		userEmail = Base64.decodeAsString(userEmail);
		User userToRemoveBeacon = usersRepo.getUserByEmail(userEmail);
		if(userToRemoveBeacon == null) { return Response.status(Response.Status.NOT_FOUND).build(); }
		
		try {
			Beacon beacon = beaconRepo.getBeaconById(beaconId);
			
			if (beacon.getUser().getId() != userToRemoveBeacon.getId()) { return Response.status(Response.Status.NOT_FOUND).build(); }
			
			userToRemoveBeacon.removeBeacon(beacon);
			boolean result = beaconRepo.removeBeacon(beacon);
			
			if(result) { return Response.status(Response.Status.OK).build(); }
			return Response.status(Response.Status.NO_CONTENT).build();
		} catch (WrongParameterException e) {
			e.printStackTrace();
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
}


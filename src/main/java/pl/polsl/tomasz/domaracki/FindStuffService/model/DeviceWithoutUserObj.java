package pl.polsl.tomasz.domaracki.FindStuffService.model;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;

public class DeviceWithoutUserObj {

	private Integer id;
	private String deviceId;
	private Integer notificationPriority;
	private Integer userId;
	
	public DeviceWithoutUserObj(Device device) {
		id = device.getId();
		deviceId = device.getDeviceId();
		notificationPriority = device.getNotificationPriority();
		userId = device.getUser().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Integer getNotificationPriority() {
		return notificationPriority;
	}

	public void setNotificationPriority(Integer notificationPriority) {
		this.notificationPriority = notificationPriority;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}

package pl.polsl.tomasz.domaracki.FindStuffService.model;

import java.sql.Timestamp;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;

public class NotificationWithourUserObj {

	private Integer id;
	private Double geoCoordinatesLatitude;
	private Double geoCoordinatesLongitude;
	private String message;
	private Integer user;
	private Timestamp notificationTime;
	
	public NotificationWithourUserObj(Notification notification) {
		id = notification.getId();
		geoCoordinatesLatitude = notification.getGeoCoordinatesLatitude();
		geoCoordinatesLongitude = notification.getGeoCoordinatesLongitude();
		message = notification.getMessage();
		notificationTime = notification.getNotificationTime();
		
		user = notification.getUser().getId();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getGeoCoordinatesLatitude() {
		return geoCoordinatesLatitude;
	}

	public void setGeoCoordinatesLatitude(Double geoCoordinatesLatitude) {
		this.geoCoordinatesLatitude = geoCoordinatesLatitude;
	}

	public Double getGeoCoordinatesLongitude() {
		return geoCoordinatesLongitude;
	}

	public void setGeoCoordinatesLongitude(Double geoCoordinatesLongitude) {
		this.geoCoordinatesLongitude = geoCoordinatesLongitude;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}

	public Timestamp getNotificationTime() {
		return notificationTime;
	}

	public void setNotificationTime(Timestamp notificationTime) {
		this.notificationTime = notificationTime;
	}
	
}

package pl.polsl.tomasz.domaracki.FindStuffService.access;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.naming.ConfigurationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONException;
import org.json.JSONObject;

import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.NotImplementedException;
import pl.polsl.tomasz.domaracki.FindStuffService.model.ClientCredential;

public class HttpAuthRequest extends HttpServletRequestWrapper {
	
	private String pass;
	private String mail;
	
	public HttpAuthRequest(HttpServletRequest request) {
		super(request);		
		String loginForToken = this.getHeader("loginForToken");
		if(loginForToken != null) {
			String [] splittedLoginData = loginForToken.split("::\\?\\?::");
			if(splittedLoginData != null && splittedLoginData.length > 1) {
				mail = splittedLoginData[0];
				pass = splittedLoginData[1];
			}
		}
	}
	
	public ClientCredential getClientCredential() {
		String token = this.getHeader("authorization");
		if(token == null){ return null; }
		
		String [] splittedAuthorization = token.split(":");
		if(splittedAuthorization == null){ return null; }
		
		if(splittedAuthorization.length > 1){ 
			ClientCredential credential = new ClientCredential();
			mail = splittedAuthorization[0];
			
			credential.setUsername( getEmail() );
			credential.setToken(splittedAuthorization[1]);
			return credential;
		}
		return null;
	}
	
	public String getEmail(){		
		if(mail == null){ return null; }
		return Base64.decodeAsString(mail);		
	}
	
	public String getPassword(){		
		if(pass == null){ return null; }
		return Base64.decodeAsString(pass);	
	}
	
}

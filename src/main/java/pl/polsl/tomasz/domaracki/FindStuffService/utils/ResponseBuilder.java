package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import java.io.IOException;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

public class ResponseBuilder {
	
	private HttpServletResponse response;
	
	public ResponseBuilder(ServletResponse response){
		this.response = (HttpServletResponse)response;
	}
	
	public ServletResponse getNoAuthorizationResponse(){
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//		try {
//			response.getOutputStream().println("No authorization");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return (ServletResponse)response;
	}
	
}

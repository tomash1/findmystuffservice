package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.glassfish.jersey.internal.util.Base64;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.decorators.BeaconCredentialExt;
import pl.polsl.tomasz.domaracki.FindStuffService.model.BeaconCredential;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.BeaconCredentialsManager;

@Path("beaconcredentials")
public class BeaconCredentialsResource {

	private IUserRepository usersRepo = new UserRepository();
	
	@GET
	@Path("/{userEmail}")
	public String getBeaconCredentials(@PathParam("userEmail") String userEmail){
		if(userEmail == null) { return null; }
		
		userEmail = Base64.decodeAsString(userEmail);
		User user = usersRepo.getUserByEmail(userEmail);
		if(user == null) { return null; } 
		
		BeaconCredential credential = new BeaconCredentialsManager(user).getBeaconCredential();
		return new BeaconCredentialExt(credential).toJSONString();
	}
	
}

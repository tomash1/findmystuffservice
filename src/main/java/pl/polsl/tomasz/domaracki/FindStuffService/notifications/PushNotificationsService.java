package pl.polsl.tomasz.domaracki.FindStuffService.notifications;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Device;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.PathsManager;

import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;

public class PushNotificationsService  {
	
	private User user;
	private ApnsService service;
	
	private String cerPass = "some6_cooks";
	
	public PushNotificationsService(User user) {
		String certPath = "/Users/tomekdomaracki/Documents/PushCert.p12";
		
		this.service = APNS.newService().withCert(certPath, cerPass).withSandboxDestination().build();
		this.user = user;
	}
	
	public void sendTestNotification(){
		String payload = APNS.newPayload().alertBody("Test Message").sound("default").build();
		pushPayload(payload);
	}	
	
	public void sendFoundStuffNotification(String message){
		String payload = APNS.newPayload().alertBody(message + " was found").sound("default").build();
		pushPayload(payload);
	}
	
	public void silentNotification(){
		String payload = APNS.newPayload().instantDeliveryOrSilentNotification().build();
		pushPayload(payload);
	}
	
	private void pushPayload(String payload){
		for(Device device : user.getDevices()){
			service.push(device.getDeviceId(), payload);
		}
	}
}

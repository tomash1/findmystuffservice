package pl.polsl.tomasz.domaracki.FindStuffService.decorators;
import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffService.model.BeaconWithoutUserObj;

public class BeaconExt {
	private Beacon _beacon;
	
	public BeaconExt(Beacon beacon){
		_beacon = beacon;
	}
	
	public BeaconExt(BeaconExt beaconExt){
		_beacon = beaconExt._beacon;
	}
	
	public String toJSONString(){
		BeaconWithoutUserObj withoutUserObjBeacon = new BeaconWithoutUserObj(_beacon);
		String jsonBeacon = new Gson().toJson(withoutUserObjBeacon);
		return jsonBeacon;
	}
	
	static public Beacon modelFromJSON(String beaconJSON){
		return new Gson().fromJson(beaconJSON, Beacon.class);
	}
}

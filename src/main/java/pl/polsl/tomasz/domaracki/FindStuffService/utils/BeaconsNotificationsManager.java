package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;
import pl.polsl.tomasz.domaracki.FindStuffService.model.DeviceNotification;

public class BeaconsNotificationsManager {
	
	List<DeviceNotification> globalNotifications = new ArrayList<>();
	
	public BeaconsNotificationsManager() {
		getNotifications();
	}
	
	public void addNewNotificationToGlobalSearch(DeviceNotification notification){
		addNotification(notification);
	}
	
	public void removeNotificationFromGlobalSearch(Integer beaconId){
		removeNotification(beaconId);
	}
	
	public List<DeviceNotification> getActiveGlobalNotifications(){
		return globalNotifications;
	}
	
	public String getActiveGlobaNotificationsJSONString(){
		List<String> jsonNotifications = new ArrayList<>();
		for(DeviceNotification notification : globalNotifications) {
			jsonNotifications.add(notification.toJSONString());
		}
		JsonElement element = new Gson().toJsonTree(jsonNotifications);

		if (! element.isJsonArray()) {
		// fail appropriately
		    String fail = "";
		    
		}
		JsonArray jsonArray = element.getAsJsonArray();
		String arrayStr = jsonArray.toString(); 
		return arrayStr;
	}

// FILE OPERATIONS
	
	private void getNotifications(){
		List<DeviceNotification> deviceNotifications = new ArrayList<>();
		List<String> notifications = readNotifications();
		for(String notification : notifications){
			try {
				DeviceNotification deviceNotification = DeviceNotification.fromString(notification);
				if(deviceNotification != null) {
					deviceNotifications.add(deviceNotification);
				}
			} catch (WrongParameterException e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		}
		globalNotifications = deviceNotifications;
	}
	
	private void removeNotification(Integer beaconId){
		for(int i = 0; i < globalNotifications.size(); i++){
			if(globalNotifications.get(i).getBeacon().getId() == beaconId){
				globalNotifications.remove(globalNotifications.get(i));
				break;
			}
		}
		saveNotifications();
	}
	
	private void addNotification(DeviceNotification notification){
		if(! globalNotificationsContains(notification)){
			globalNotifications.add(notification);
			saveNotifications();
		}
	}
	
	private boolean globalNotificationsContains(DeviceNotification notification){
		for(DeviceNotification notif : globalNotifications){
			if(notif.getBeacon().getId() == notification.getBeacon().getId()){
				return true;
			}
		}
		return false;
	}
	
	private List<String> readNotifications(){
		List<String> uuids = new ArrayList<>();
		try {
			uuids = Files.readAllLines(PathsManager.getNotificationsFilePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return uuids;
	}
	
	private void saveNotifications(){
		List<String> toSaveLines = new ArrayList<>();
		for(DeviceNotification notification : globalNotifications){
			toSaveLines.add( notification.toFileString() );
		}
		try {
			Files.write(PathsManager.getNotificationsFilePath(), toSaveLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathsManager {
	
	static public Path getServerPath() {
		return Paths.get("/Users/tomekdomaracki/Documents").toAbsolutePath();
	}
	
	static public Path getGlobalCredentialsFilePath(){
		return getServerPath().resolve("beaconsCredentials.txt");
	}
	
	static public Path getNotificationsFilePath(){
		return getServerPath().resolve("beaconsNotifications.txt");
	}
	
	static public Path getDevicesFilePath(){
		return getServerPath().resolve("userDevicesIDs.txt");
	}

	static public String getServerPathString() {
		Path path = Paths.get("").toAbsolutePath();
		File file = path.toFile();
		return file.getAbsolutePath();
	}
	
	static public String getNotificationsCertificatePath(){
		String servicePath = getServerPathString();
		
		return null;
	}
}

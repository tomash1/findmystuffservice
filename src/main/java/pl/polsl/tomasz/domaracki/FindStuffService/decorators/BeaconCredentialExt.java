package pl.polsl.tomasz.domaracki.FindStuffService.decorators;

import org.json.JSONObject;

import pl.polsl.tomasz.domaracki.FindStuffService.model.BeaconCredential;

public class BeaconCredentialExt {
	
	public BeaconCredential Credential;
	
	public BeaconCredentialExt(BeaconCredential beaconCredential){
		Credential = beaconCredential;
	}
	
	public BeaconCredentialExt(BeaconCredentialExt beaconCredentialExt){
		Credential = beaconCredentialExt.Credential;
	}
	
	public String toJSONString(){
		JSONObject jsonBeaconCredential = new JSONObject(Credential);
		return jsonBeaconCredential.toString();
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.model;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IBeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.BeaconRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.exceptions.WrongParameterException;

public class DeviceNotification {

	private static IBeaconRepository beaconRepo = new BeaconRepository();
	
	public class CONSTS {
		public static final int BEACON_ID = 0;
		public static final int MESSAGE	  = 1;
	}
	
	private String message;
	private Beacon beacon;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Beacon getBeacon() {
		return beacon;
	}
	public void setBeacon(Beacon beacon) {
		this.beacon = beacon;
	}
	
	public String toJSONString() {
		DeviceNotificationWithNoUserBeacon notif = new DeviceNotificationWithNoUserBeacon(this);
		return new Gson().toJson(notif);
	}
	
	public String toFileString() {
		return 	beacon.getId().toString() +
				";" +
				message;
	}
	
	public static DeviceNotification fromJSON(String notificationJSON) throws WrongParameterException{
		DeviceNotification notification = new Gson().fromJson(notificationJSON, DeviceNotification.class);
		if(notification == null){
			throw new WrongParameterException("DeviceNotification failed to create from JSON String");
		}
		else {
			return notification;
		}
	}
	
	public static DeviceNotification fromString(String notification) throws WrongParameterException{
		String [] notificationSplitted = notification.split(";");
		if(notificationSplitted.length > 1){
			Integer beaconId = Integer.parseInt(notificationSplitted[DeviceNotification.CONSTS.BEACON_ID]);
			Beacon beacon = beaconRepo.getBeaconById(beaconId);
			if (beacon == null){
				throw new WrongParameterException("Given beacon does not exist");
			}
			
			DeviceNotification deviceNotification = new DeviceNotification();
			deviceNotification.setBeacon(beacon);
			deviceNotification.setMessage(notificationSplitted[DeviceNotification.CONSTS.MESSAGE]);
			return deviceNotification;
		}
		return null;
		//throw new WrongParameterException("Device notification string has wrong format");
	}
}

class DeviceNotificationWithNoUserBeacon{
	
	private String message;
	private BeaconWithoutUserObj beacon;
	
	public DeviceNotificationWithNoUserBeacon(DeviceNotification notification) {
		message = notification.getMessage();
		beacon = new BeaconWithoutUserObj(notification.getBeacon());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BeaconWithoutUserObj getBeacon() {
		return beacon;
	}

	public void setBeacon(BeaconWithoutUserObj beacon) {
		this.beacon = beacon;
	}
	
}

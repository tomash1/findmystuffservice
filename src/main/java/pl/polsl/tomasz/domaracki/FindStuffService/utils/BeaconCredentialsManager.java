package pl.polsl.tomasz.domaracki.FindStuffService.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Beacon;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffService.model.BeaconCredential;

public class BeaconCredentialsManager {
	
	private final int MAX_USER_PER_UUID = 65;
	
	private List<UUIDAndMajorId> globalCredentials = new ArrayList<>();
	private User user;
	
	public BeaconCredentialsManager(User user) {
		this.user = user;
		
		getUUIDs();
	}
	
	public BeaconCredential getBeaconCredential() {
		UUIDAndMajorId userCredentials = getUserCredentials();
		
		BeaconCredential credential = new BeaconCredential();
		credential.setMinorId( String.valueOf( user.getBeacons().size() + 1 ) );
		credential.setMajorId( String.valueOf( userCredentials.majorId.toString() ));
		credential.setUuid   ( userCredentials.uuid );
		return credential;
	}
	
	
//USER CREDENTIALS
	
	private UUIDAndMajorId getUserCredentials(){
		UUIDAndMajorId credential;
		if(user.getBeacons().size() > 0){
			credential = getNextUserCredentials();
		} else {
			credential = getNextGlobalCredentials();
		}
		return credential;
	}
	
	private UUIDAndMajorId getNextUserCredentials() {
		return new UUIDAndMajorId(
				user.getBeacons().iterator().next().getUuid(), 
				user.getBeacons().iterator().next().getMajorId()
				);
	}

// GLOBAL CREDENTIALS
	
	private UUIDAndMajorId getNextGlobalCredentials() {
		//return "79EAA41C-D842-4F23-963D-BC55087B883F";
		UUIDAndMajorId credential = getFreeCredential();
		return credential;
	}
	
	private UUIDAndMajorId getFreeCredential(){		
		for(UUIDAndMajorId credential : globalCredentials){
			if (credential.majorId < MAX_USER_PER_UUID) {
				credential.majorId++;
				saveCredentials();
				return credential;
			}
		}
		return prepareNewUUID();
	}
	
	private UUIDAndMajorId prepareNewUUID(){
		String newUUID = UUID.randomUUID().toString();
		UUIDAndMajorId credential = new UUIDAndMajorId(newUUID, 1);
		
		addCredential(credential);
		saveCredentials();
		return credential;
	}
	
// FILE OPERATIONS
	
	private void getUUIDs(){
		List<UUIDAndMajorId> uuidsAndMajorIds = new ArrayList<>();
		List<String> uuidsFromFile = readUUIDs();
		for(String uuid : uuidsFromFile){
			String [] uuidAndMajorSplitted = uuid.split(";");
			if(uuidAndMajorSplitted.length > 1){
				UUIDAndMajorId uuidWithMajor = new UUIDAndMajorId(
									uuidAndMajorSplitted[0], 
									uuidAndMajorSplitted[1]
								);
				uuidsAndMajorIds.add(uuidWithMajor);
			}
		}
		globalCredentials = uuidsAndMajorIds;
	}
	
	private void addCredential(UUIDAndMajorId credential){
		globalCredentials.add(credential);
	}
	
	private List<String> readUUIDs(){
		List<String> uuids = new ArrayList<>();
		try {
			uuids = Files.readAllLines(PathsManager.getGlobalCredentialsFilePath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return uuids;
	}
	
	private void saveCredentials(){
		List<String> toSaveLines = new ArrayList<>();
		for(UUIDAndMajorId credential : globalCredentials){
			String toSave = convertUUIDtoSave(credential);
			toSaveLines.add(toSave);
		}
		try {
			Files.write(PathsManager.getGlobalCredentialsFilePath(), toSaveLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String convertUUIDtoSave(UUIDAndMajorId credential){
		return credential.uuid + ";" + credential.majorId.toString() + "\n";
	}
}

class UUIDAndMajorId {
	public String uuid;
	public Integer majorId;
	
	UUIDAndMajorId(String value, String majorId){
		this.uuid 	 = value;
		this.majorId = Integer.parseInt(majorId);
	}
	
	UUIDAndMajorId(String value, Integer majorId){
		this.uuid 	 = value;
		this.majorId = majorId;
	}
}
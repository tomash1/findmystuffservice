package pl.polsl.tomasz.domaracki.FindStuffService.exceptions;

public class WrongParameterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;
	
	public WrongParameterException() {}
	
	public WrongParameterException(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}
	
}


package pl.polsl.tomasz.domaracki.FindStuffService.decorators;

import com.google.gson.Gson;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.Notification;
import pl.polsl.tomasz.domaracki.FindStuffService.model.NotificationWithourUserObj;

public class NotificationExt {

	private Notification _notification;
	
	public NotificationExt(Notification notification){
		_notification = notification;
	}
	
	public NotificationExt(NotificationExt notificationExt){
		_notification = notificationExt._notification;
	}
	
	public String toJSONString(){
		NotificationWithourUserObj withoutUserObjNotification = new NotificationWithourUserObj(_notification);
		String jsonNotification = new Gson().toJson(withoutUserObjNotification);
		return jsonNotification;
	}
	
	static public Notification modelFromJSON(String notificationJSON){
		return new Gson().fromJson(notificationJSON, Notification.class);
	}
}

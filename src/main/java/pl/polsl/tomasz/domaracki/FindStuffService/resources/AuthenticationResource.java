package pl.polsl.tomasz.domaracki.FindStuffService.resources;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.access.HttpAuthRequest;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.TokenBuilder;

@Path("authentication")
public class AuthenticationResource {

	private HttpAuthRequest authRequest;
	IUserRepository userRepository = new UserRepository();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getAuthenticationToken(@Context HttpServletRequest request){
		//authRequest = new HttpAuthRequest(request);
		String userMail = (String)request.getAttribute("username");
		if(userMail == null) { return ""; }
		
		User userRequestingToken = userRepository.getUserByEmail(userMail);
		if(userRequestingToken == null ) { return ""; }
		
		TokenBuilder tokenBuilder = new TokenBuilder(userRequestingToken);
		return tokenBuilder.getUserToken();		
	}
}

package pl.polsl.tomasz.domaracki.FindStuffService.access;

import javax.naming.ConfigurationException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.SecureHash;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.ISaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.IUserRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.SaltRepository;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;
import pl.polsl.tomasz.domaracki.FindStuffService.model.ClientCredential;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.ServiceUserBuilder;
import pl.polsl.tomasz.domaracki.FindStuffService.utils.TokenBuilder;

public class Authenticator {
	private IUserRepository repository;
	private User userToAuthenticate;
	private User userFromDb;	
	
	private ServletRequest originalRequest;
	private HttpAuthRequest request;	
	
	private String email;
	private ClientCredential credential;
	
	public Authenticator(ServletRequest req){
		originalRequest = req;
		request = new HttpAuthRequest( (HttpServletRequest)req );
		repository = new UserRepository();
		initAuthenticator();
	}
	
	private void initAuthenticator(){
		email = request.getEmail();
		if(email != null){
			String pass  = request.getPassword();
			if(pass == null){ return; }	
			
			userToAuthenticate = new ServiceUserBuilder()
								.withEmail(email)
								.withStringPass(pass)
								.build();
			userFromDb = repository.getUserByEmail(userToAuthenticate.getEmail());
		}
		else{	
			credential = request.getClientCredential();
			if(credential == null) { return; }
			
			userToAuthenticate = new ServiceUserBuilder()
								.withEmail(credential.getUsername())
								.build();
			userFromDb = repository.getUserByEmail(userToAuthenticate.getEmail());
		}
	}
	
	public boolean isNotificationPath(){
		String path = request.getPathInfo();
		if (path != null && path.startsWith("/devicenotifications")){
			return true;
		}
		return false;
	}
	
	public boolean isCreateUserPath(){
		String path = request.getPathInfo();
		if (path != null && path.startsWith("/users")){
			String method = request.getMethod();
			if (request.getMethod().equals("POST") ) {
				return true;
			}
		}
		return false;
	}
	
	public boolean authenticate(){
		
		if( userFromDb == null )	{ return false; }
		if( !checkUserSalt() )		{ return false; }
		if( !checkGivenPassword() )	{ return false; }
		
		originalRequest.setAttribute("username", email);
		return true;
	}
	
	public boolean authenticateToken() throws ConfigurationException{
		if( userFromDb == null ) { return false; }
		if( credential == null ) { throw new ConfigurationException(); }
		if( !checkToken() ) { return false; }
		
		originalRequest.setAttribute("username", credential.getUsername());
		return true;
	}
	
	private boolean checkUserSalt(){
		ISaltRepository saltRepo = new SaltRepository();
		SecureHash hash = saltRepo.getHashByUser(userFromDb);
		
		if( hash == null ){ return false; }
		if( hash.getSalt() != null ){ return true; }
		return false;
	}
	
	private boolean checkGivenPassword(){		
		if( userFromDb.getPasswordHash().equals( userToAuthenticate.getPasswordHash() )){
			return true;
		}
		return false;
	}	
	
	private boolean checkToken(){
		TokenBuilder builder = new TokenBuilder(userFromDb);
		boolean result = builder.checkAuthorizationToken(credential.getToken());
		if(result) { return true; }
		return false;
	}
}

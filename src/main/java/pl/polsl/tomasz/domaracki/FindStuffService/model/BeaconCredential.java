package pl.polsl.tomasz.domaracki.FindStuffService.model;

public class BeaconCredential {

	private String majorId;
	private String minorId;
	private String uuid;
	
	public String getMajorId() {
		return majorId;
	}
	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}
	public String getMinorId() {
		return minorId;
	}
	public void setMinorId(String minorId) {
		this.minorId = minorId;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}	
}

package pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.model.User;
import pl.polsl.tomasz.domaracki.FindStuffDatabaseAgent.repository.implementation.UserRepository;

public class UserRepositoryTest {
	private static IUserRepository repository;
	private static User user;
	
	@BeforeClass
	public static void setUp() {
		repository = new UserRepository();
		
		user = new User();
		user.setEmail("testEmail@email.com");
		user.setBeacons(null);
		user.setNotifications(null);
		user.setPasswordHash("hash");
	}
	
	@AfterClass
	public static void tearDown(){
		//repository.removeUser(user);
	}
	
	@Test
	public void testAddUser() {		
		assertNotNull(repository.addUser(user));
		user = repository.getUserByEmail("testEmail@email.com");
	}
	
	@Test
	public void testAddNullUser() {
		assertNull(repository.addUser(null));
	}

	@Test
	public void testGetUserByEmailExist() {
		User user = repository.getUserByEmail("tomdomaracki@gmail.com");
		
		assertEquals("tomdomaracki@gmail.com", user.getEmail());
	}
	
	@Test
	public void testGetUserByEmailNotExist() {
		User user = repository.getUserByEmail("");
		
		assertNull(user);
	}

	@Test
	public void testResetGivenUserPassword() {
		User userEmail = new User();
		userEmail.setEmail("tomdomaracki@gmail.com");
		userEmail.setPasswordHash("pass");
		
		assertTrue(repository.resetGivenUserPassword(userEmail));
	}

	@Test
	public void testRemoveUser() {
		
		assertTrue(repository.removeUser(user));
	}

	@Test
	public void testRemoveNullUser() {
		assertFalse(repository.removeUser(null));
	}	
}
